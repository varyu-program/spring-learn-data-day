package com.gavin.timeid.utils;

public class TimeIdUtils {

    //同步锁
    private static final Class<TimeIdUtils> LOCK = TimeIdUtils.class;


    public static long timeId(){
        synchronized (LOCK){
            long result = System.nanoTime();
            while(true){
                long current = System.nanoTime();
                if (current-result>1){
                    return result;
                }
            }
        }
    }

}
