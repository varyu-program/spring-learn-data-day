package com.gavin.redisid.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RedisIdController {

    private String lua =
// 获取是否启用标志
            " local start = redis.call('hget', KEYS[1],'start') \n"
// 如果未启用
                    + "if tonumber(start) == 0 then \n"
//获取开始值
                    + " local result = redis.call('hget', KEYS[1], 'offset') \n"
//将当前值设置为开始值
                    + " redis.call('hset', KEYS[1], 'current', result) \n"
//将是否启用标志设置为已经启用（1）
                    + " redis.call('hset', KEYS[1],'start','1') \n"
// 返回结束
                    + " return result \n"
//结束if语句
                    + " end \n"
                    // 获取当前值
                    + " local current = redis.call('hget', KEYS[1], 'current') \n"

// 获取步长
                    + " local step = redis.call('hget', KEYS[1], 'step') \n"
//结算新的当前值
                    + " local result = current + step \n"
// 设置新的当前值
                    + " redis.call('hset', KEYS[1], 'current', result) \n"
// 返回结果
                    + " return result \n";



    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping("/id/{keyType}")
    public String getKey(@PathVariable("keyType") String keyType) {
        DefaultRedisScript<Long> rs = new DefaultRedisScript<Long>();
        rs.setScriptText(lua);
        rs.setResultType(Long.class);
        //定义脚本key参数
        List<String> keyList = new ArrayList<>();
        keyList.add(keyType);
        //执行脚本,传递参数
        Long aLong = redisTemplate.execute(rs, keyList);
        return aLong.toString();
    }
}
