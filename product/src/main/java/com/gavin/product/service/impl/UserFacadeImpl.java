package com.gavin.product.service.impl;

import com.gavin.common.ResultMessage;
import com.gavin.pojo.UserInfo;
import com.gavin.product.hystrix.UserExpCommand;
import com.gavin.product.hystrix.UserGetCommand;
import com.gavin.product.hystrix.UserPutCommand;
import com.gavin.product.hystrix.UserTimeoutCommand;
import com.gavin.product.service.UserFacade;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixObservableCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.annotation.ObservableExecutionMode;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheKey;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheRemove;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import rx.Observable;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class UserFacadeImpl implements UserFacade {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    @HystrixCommand(fallbackMethod = "fallback1")//当调用出现超时时时会走下面的回调函数
    public ResultMessage timeout() {
        String url = "http://USER/hystrix/timeout";
        return restTemplate.getForObject(url, ResultMessage.class);
    }

    @Override
    @HystrixCommand(fallbackMethod = "fallback2")//当调用出现异常时会走下面的回调函数
    public ResultMessage exp(String msg) {
        String url = "http://USER/hystrix/exp/{msg}";
        return restTemplate.getForObject(url, ResultMessage.class, msg);
    }


    public ResultMessage fallback1() {
        return new ResultMessage(false, "超时了");

    }

    public ResultMessage fallback2(String msg) {
        return new ResultMessage(false, "调用产生异常了,参数:" + msg);
    }

    @Override
    public ResultMessage timeout2() {
        //创建一个HystrixCommandGroupKey 对象,将HystrixCommand进行分组
        HystrixCommandGroupKey userGroup = HystrixCommandGroupKey.Factory.asKey("userGroup");
        //为构建UserTimeoutCommand做准备
        com.netflix.hystrix.HystrixCommand.Setter setter = com.netflix.hystrix.HystrixCommand.Setter.withGroupKey(userGroup);
        //构建对象
        UserTimeoutCommand userTimeoutCommand = new UserTimeoutCommand(setter, restTemplate);
        //同步执行命令
        return userTimeoutCommand.execute();
        /**
         * 异步执行命令
         */
//        Future<ResultMessage> queue = userTimeoutCommand.queue(); //进入发送队列并返回一个Future对象,到这里并未向服务发送请求
//        try {
//            return  queue.get();  //到这里发送请求并获得返回值
//        } catch (Exception e) {
//             return userTimeoutCommand.getFallback();
//        }
    }

    /**
     * 异步执行
     *
     * @return
     */
    @Override
    @HystrixCommand(fallbackMethod = "fallback1")
    public Future<ResultMessage> asyncTimeout() {
        return new AsyncResult<ResultMessage>() {
            @Override
            public ResultMessage invoke() {
                String url = "http://USER/hystrix/timeout";
                return restTemplate.getForObject(url, ResultMessage.class);
            }
        };
    }

    @Override
    public List<ResultMessage> exp2(String[] params) {
        HystrixCommandGroupKey userGroup = HystrixCommandGroupKey.Factory.asKey("userGroup");
        HystrixObservableCommand.Setter setter = HystrixObservableCommand.Setter.withGroupKey(userGroup);
        UserExpCommand userExpCommand = new UserExpCommand(setter, restTemplate, params);
        List<ResultMessage> resList = new ArrayList<>();
        //热观察者模式,立刻执行描述的行为,
        Observable<ResultMessage> observe = userExpCommand.observe();
        //冷观察者模式,延迟描述行为
        //Observable<ResultMessage> resultMessageObservable = userExpCommand.toObservable();
        observe.forEach((ResultMessage resultMsg) -> {

            resList.add(resultMsg);//在冷观察这模式下,执行到这里才会发送请求服务
        });
        //同步执行命令
        return resList;
    }


    /**
     * 指定饥饿加载---observableExecutionMode = ObservableExecutionMode.EAGER
     * 热观察者模式,立刻执行描述的行为,
     * Observable<ResultMessage> observe = userExpCommand.observe();
     * 懒加载模式observableExecutionMode = ObservableExecutionMode.LAZY
     * //Observable<ResultMessage> resultMessageObservable = userExpCommand.toObservable();
     * observe.forEach((ResultMessage resultMsg) -> {
     * resList.add(resultMsg);//在冷观察这模式下,执行到这里才会发送请求服务
     * });
     *
     * @param params
     * @return
     */
    @HystrixCommand(fallbackMethod = "fallback3",
            observableExecutionMode = ObservableExecutionMode.EAGER,
            ignoreExceptions = FileNotFoundException.class)
    @Override
    public Observable<ResultMessage> asyncExp(String[] params) {
        String url = "http://USER/hystrix/exp/{msg}";
        Observable.OnSubscribe<ResultMessage> onSubs = (resSubs) -> {

            try {
                int count = 0;//计数
                if (!resSubs.isUnsubscribed()) {
                    for (String param : params) {
                        count++;
                        System.out.println("第[" + count + "]次发送");
                        //发送参数
                        ResultMessage resMsg = restTemplate.getForObject(url, ResultMessage.class, param);
                        resSubs.onNext(resMsg);
                    }
                    //遍历所有参数后,发射完毕
                    resSubs.onCompleted();
                }
            } catch (Exception e) {
                resSubs.onError(e);
            }
        };
        return Observable.create(onSubs);
    }

    public ResultMessage fallback3(String[] msg) {
        return new ResultMessage(false, "调用产生异常了,参数:" + msg);
    }

    /**
     * ignoreExceptions  忽略异常,将该异常抛出不执行降级方法
     *
     * @param msg
     * @return
     */
    @Override
    @HystrixCommand(fallbackMethod = "fallback4", ignoreExceptions = RuntimeException.class)//当调用出现异常时会走下面的回调函数
    public ResultMessage ignoreExp(String msg) {
        String url = "http://USER/hystrix/exp/{msg}";
        return restTemplate.getForObject(url, ResultMessage.class, msg);
    }

    /**
     * 此处的参数为Throwable,不可以写为Exception
     *
     * @param msg
     * @param exp
     * @return
     */
    public ResultMessage fallback4(String msg, Throwable exp) {
        exp.printStackTrace();
        return new ResultMessage(false, "调用产生异常了,参数:" + msg);
    }

    @Override
    public UserInfo testUserInfo(Long id) {
        //使用hystrix上下文对象,最后关闭---这表示此次请求在一个HystrixRequestContext对象范围内有效
        HystrixRequestContext hystrixRequestContext = HystrixRequestContext.initializeContext();
        UserGetCommand userGetCommand = new UserGetCommand(id, restTemplate);
        UserGetCommand userGetCommand1 = new UserGetCommand(id, restTemplate);
        UserGetCommand userGetCommand2 = new UserGetCommand(id, restTemplate);

        UserPutCommand userPutCommand = new UserPutCommand(restTemplate, 1L, "USER_NAME_UPDATE", "NOTE_UPDATE");
        //第一次请求
        try {
            userGetCommand.execute();
            //第二次从缓存中读取
            userGetCommand1.execute();
            //执行更新
            userPutCommand.execute();
            return userGetCommand2.execute();
        } finally {
            //关闭上下文
            hystrixRequestContext.close();
        }
    }

    /**
     * @CacheKey将参数id设置为缓存key
     */
    @CacheResult  //将结果缓存,默认情况下 Hystrix命令的键为方法名,后续移除时只要指定键为方法名即可
    @HystrixCommand
    @Override
    public UserInfo getUserInfo(@CacheKey Long id) {
        String url = "http://USER/user/info/{id}";
        System.out.println("获取用户_" + id);
        return restTemplate.getForObject(url, UserInfo.class, id);
    }

    @CacheRemove(commandKey = "getUserInfo")
    @HystrixCommand
    @Override
    public UserInfo updateUserInfo(@CacheKey("id") UserInfo user) {
        String url = "http://USER/user/info";
        HttpHeaders httpHeaders = new HttpHeaders();
        //设置请求头
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<UserInfo> userInfoHttpEntity = new HttpEntity<>(user, httpHeaders);
        System.out.println("执行更新_" + user.getId());
        restTemplate.put(url, userInfoHttpEntity);
        return user;
    }

    /**
     * 自定义缓存键生成方式
     */
    private static final String CACHE_PREFIX = "user_";

    public String getCacheKey(Long id) {
        return CACHE_PREFIX + id;
    }

    public String getCacheKey(UserInfo userInfo) {
        return CACHE_PREFIX + userInfo.getId();
    }

    /**
     * @CacheKey将参数id设置为缓存key
     */
    @CacheResult(cacheKeyMethod = "getCacheKey")  //将结果缓存,默认情况下 Hystrix命令的键为方法名,后续移除时只要指定键为方法名即可
    @HystrixCommand(commandKey = "user_get")
    @Override
    public UserInfo getUserInfo1(@CacheKey Long id) {
        String url = "http://USER/user/info/{id}";
        System.out.println("获取用户_" + id);
        return restTemplate.getForObject(url, UserInfo.class, id);
    }

    @CacheRemove(commandKey = "user_get", cacheKeyMethod = "getCacheKey")
    @HystrixCommand
    @Override
    public UserInfo updateUserInfo2(@CacheKey("id") UserInfo user) {
        String url = "http://USER/user/info";
        HttpHeaders httpHeaders = new HttpHeaders();
        //设置请求头
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<UserInfo> userInfoHttpEntity = new HttpEntity<>(user, httpHeaders);
        System.out.println("执行更新_" + user.getId());
        restTemplate.put(url, userInfoHttpEntity);
        return user;
    }

    @Override
    public UserInfo getUser(Long id) {
        String url = "http://USER/user/info/{id}";
        return restTemplate.getForObject(url, UserInfo.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UserInfo> findUsers(Long[] ids) {
        String url = "http://USER/user/infoes/{ids}";
        String strIds = StringUtils.join(ids, ",");
        System.out.println("准备批量发送请求>>" + strIds);
        //定义转换最终类型
        ParameterizedTypeReference<List<UserInfo>> reponseType = new ParameterizedTypeReference<List<UserInfo>>() {
        };
        ResponseEntity<List<UserInfo>> userEntity = restTemplate.exchange(url, HttpMethod.GET, null, reponseType, strIds);
        return userEntity.getBody();
    }


    @HystrixCollapser(collapserKey = "userGroup",
            //指定合并方法
            batchMethod = "findUser2",
            //设置作用域
            scope = com.netflix.hystrix.HystrixCollapser.Scope.GLOBAL,
//            设置合并的配置
            collapserProperties = {
                    @HystrixProperty(name = "timerDelayInMilliseconds", value = "50"),
                    @HystrixProperty(name = "maxRequestsInBatch", value = "3")})
   //方法只需要声明,不需要做别的
    @Override
    public Future<UserInfo> getUser2(Long id) {
        return null;
    }

    @HystrixCommand(commandKey = "userGroup")
    @SuppressWarnings("unchecked")
    @Override
    public List<UserInfo> findUser2(List<Long> ids) {
        String url = "http://USER/user/infoes/{ids}";
        String strArr = StringUtils.join(ids, ",");
        System.out.println("批量发送请求(注解式)" + strArr);
        ParameterizedTypeReference<List<UserInfo>> parameterizedTypeReference = new ParameterizedTypeReference<List<UserInfo>>() {
        };
        ResponseEntity<List<UserInfo>> exchange = restTemplate.exchange(url, HttpMethod.GET, null, parameterizedTypeReference, strArr);
        return exchange.getBody();
    }
}
