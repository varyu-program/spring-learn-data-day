package com.gavin.product.service;

import com.gavin.common.ResultMessage;
import com.gavin.pojo.UserInfo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheKey;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheRemove;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import rx.Observable;

import java.util.List;
import java.util.concurrent.Future;

public interface UserFacade {

    public ResultMessage timeout();

    public ResultMessage timeout2();

    public ResultMessage exp(String msg);

    public List<ResultMessage> exp2(String[] params);

    public Future<ResultMessage> asyncTimeout();

    public Observable<ResultMessage> asyncExp(String[] params);

    public ResultMessage ignoreExp(String msg);

    UserInfo testUserInfo(Long id);

    public UserInfo getUserInfo(Long id);


    UserInfo updateUserInfo(UserInfo user);


    UserInfo getUserInfo1( Long id);


    UserInfo updateUserInfo2(UserInfo user);

    UserInfo getUser(Long id);

    List<UserInfo> findUsers(Long[] ids);

    Future<UserInfo>getUser2(Long id);

    @HystrixCommand(commandKey = "userGroup")
    @SuppressWarnings("unchecked")
    List<UserInfo> findUser2(List<Long> ids);
}
