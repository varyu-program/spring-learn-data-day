package com.gavin.product.productServer;

import com.gavin.common.ResultMessage;
import com.gavin.pojo.UserInfo;
import com.gavin.product.hystrix.collapser.UserHystrixCollapaser;
import com.gavin.product.service.UserFacade;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
public class HystrixController {

    @Autowired
    private UserFacade userFacade;

    @GetMapping("/cr/timeout")
    public ResultMessage timeout() {
        return userFacade.timeout();
    }


    @GetMapping("/cr/exp/{msg}")
    public ResultMessage exp(@PathVariable("msg") String msg) {
        return userFacade.exp(msg);
    }

    @GetMapping("/cr/timeout2")
    public ResultMessage timeout2() {
        return userFacade.timeout2();
    }

    @GetMapping("/user/info/{id}")
    public UserInfo getUserInfo(@PathVariable("id") Long id) {
        return userFacade.testUserInfo(id);
    }

    @GetMapping("/user/info/cache/{id}")
    public UserInfo getUserInfo2(@PathVariable Long id) {
        //初始化上下文
        HystrixRequestContext hystrixRequestContext = HystrixRequestContext.initializeContext();
        try {
            userFacade.getUserInfo(id);
            userFacade.getUserInfo(id);
            UserInfo userInfo = new UserInfo(id, "user_name_update", "note_update");
            userFacade.updateUserInfo(userInfo);
            return userFacade.getUserInfo(id);
        } finally {
            hystrixRequestContext.close();
        }
    }

    /**
     * 合并请求demo
     * @param ids
     * @return
     */
    @GetMapping("/user/infoes/{ids}")
    public List<UserInfo> findUsers(@PathVariable("ids") Long[] ids) {
        try {
            List<UserInfo> userInfoList = new ArrayList<>(ids.length);

            List<Future<UserInfo>> futureList = new ArrayList<>(ids.length);
//将请求全部放入队列
            for (Long id : ids) {
                //这里使用queue,异步请求
                Future<UserInfo> queue = new UserHystrixCollapaser(userFacade, id).queue();
                futureList.add(queue);
            }

            //合并请求,获取结果
            for (Future<UserInfo> userInfoFuture : futureList) {
                userInfoList.add(userInfoFuture.get());
            }
            return userInfoList;
//            userFacade.findUsers(ids);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

 @GetMapping("/user/infoes2/{ids}")
    public List<UserInfo> findUsers2(@PathVariable("ids") Long[] ids) {

            List<UserInfo> userInfoList = new ArrayList<>(ids.length);

            List<Future<UserInfo>> futureList = new ArrayList<>(ids.length);
//将请求全部放入队列
            for (Long id : ids) {
                //这里使用queue,异步请求
                Future<UserInfo> queue = new UserHystrixCollapaser(userFacade, id).queue();
                futureList.add(queue);
            }

            //合并请求,获取结果
            for (Future<UserInfo> userInfoFuture : futureList) {
                try {
                userInfoList.add(userInfoFuture.get());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return userInfoList;

    }


}
