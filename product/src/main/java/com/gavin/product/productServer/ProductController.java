package com.gavin.product.productServer;

import com.gavin.common.ResultMessage;
import com.gavin.pojo.UserInfo;
import com.gavin.product.hystrix.collapser.UserHystrixCollapaser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/purchase/{userId}/{productId}/{amount}")
    public ResultMessage purchaseProduct(@PathVariable("userId") Long userId,
                                         @PathVariable("productId") Long productId,
                                         @PathVariable("amount") Double amount) {
        System.out.println("扣减产品--");
        //FUND代表fund微服务,调用fund中的服务
        String url = "http://FUND/fund/account/balance/{userId}/{amount}";
        //封装请求参数
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("amount", amount);
        ResultMessage resultMessage = restTemplate.postForObject(url, null, ResultMessage.class, params);
        System.out.println(resultMessage.getMessage());
        System.out.println("数据库已记录交易信息");
        return new ResultMessage(true, "交易成功");
    }




}
