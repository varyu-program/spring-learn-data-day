package com.gavin.product.filter;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


/**
 * 为了减少上下文生成的代码数量,(因为要使用缓存,必须在一个上下文对象的范围中)
 * 配置一个过滤器,在请求url时统一生成一个HystrixRequestContext上下文对象
 *
 */
@WebFilter(filterName ="HystrixRequestContextFilter",urlPatterns = "/user/info/cache/*")
@Component
public class HystrixRequestContextFilter  implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HystrixRequestContext hystrixRequestContext = HystrixRequestContext.initializeContext();
        try {
            filterChain.doFilter(servletRequest,servletResponse);
        } finally {
            hystrixRequestContext.close();
        }

    }
}
