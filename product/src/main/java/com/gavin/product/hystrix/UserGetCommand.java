package com.gavin.product.hystrix;

import com.gavin.pojo.UserInfo;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixRequestCache;
import com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategyDefault;
import org.springframework.web.client.RestTemplate;

public class UserGetCommand extends HystrixCommand<UserInfo> {

    private Long id;

    private RestTemplate restTemplate;
    String url = "http://USER/user/info/{id}";

    private static final HystrixCommandKey commandkey = HystrixCommandKey.Factory.asKey("user_get");

    public UserGetCommand(Long id, RestTemplate restTemplate) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("userGroup")).andCommandKey(commandkey));
        this.id = id;
        this.restTemplate = restTemplate;
    }

    @Override
    protected UserInfo run() throws Exception {
        System.out.println("获取用户信息_" + id);
        return restTemplate.getForObject(url, UserInfo.class, id);
    }

    /**
     * 通过缓存键
     *
     * @return
     */
    @Override
    protected String getCacheKey() {
        return "user_" + id;
    }

    public static void clearCache(Long id) {
        String cacheKey = "user_" + id;
        HystrixRequestCache.getInstance(commandkey,
                HystrixConcurrencyStrategyDefault.getInstance()).clear(cacheKey);
    }


}
