package com.gavin.product.hystrix;

import com.gavin.pojo.UserInfo;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

/**
 * hystrix缓存
 */
public class UserPutCommand extends HystrixCommand<UserInfo> {

    private UserInfo userInfo;
    private RestTemplate restTemplate;
    String url = "http://USER/user/info";

    public UserPutCommand(RestTemplate restTemplate, Long id, String userName, String note) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("userGroup")));
        userInfo = new UserInfo(id, userName, note);
        this.restTemplate = restTemplate;
    }

    @Override
    protected UserInfo run() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<UserInfo> httpEntity = new HttpEntity<>(userInfo, headers);
        System.out.println("执行了用户更新_" + userInfo.getId());
        //更新用户信息
        restTemplate.put(url, httpEntity);
        //清除缓存
        UserGetCommand.clearCache(userInfo.getId());
        return userInfo;
    }
}
