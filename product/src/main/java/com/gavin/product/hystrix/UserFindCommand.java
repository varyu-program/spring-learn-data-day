package com.gavin.product.hystrix;

import com.gavin.pojo.UserInfo;
import com.gavin.product.service.UserFacade;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

import java.util.List;

public class UserFindCommand extends HystrixCommand<List<UserInfo>> {


    private Long[] ids;
    private UserFacade userFacade;

    public UserFindCommand(UserFacade userFacade, Long[] ids) {
        super(HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("userGroup")));
        this.userFacade = userFacade;
        this.ids = ids;

    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<UserInfo> run() throws Exception {
        List<UserInfo> users = userFacade.findUsers(ids);
        return users;
    }
}
