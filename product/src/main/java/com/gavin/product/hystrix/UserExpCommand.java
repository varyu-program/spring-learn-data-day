package com.gavin.product.hystrix;

import com.gavin.common.ResultMessage;
import com.netflix.hystrix.HystrixObservableCommand;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import rx.Observable;
import rx.Subscriber;

public class UserExpCommand extends HystrixObservableCommand<ResultMessage> {

    private RestTemplate restTemplate;
    private String[] params;
    String url = "http://USER/hystrix/exp/{msg}";

    /**
     * 构造方法
     *
     * @param setter
     * @param restTemplate
     * @param params
     */
    public UserExpCommand(Setter setter, RestTemplate restTemplate, String[] params) {
        super(setter);
        this.restTemplate = restTemplate;
        this.params = params;

    }

    /**
     * 核心方法,发送参数
     *
     * @return
     */
    @Override
    protected Observable<ResultMessage> construct() {
        Observable.OnSubscribe<ResultMessage> subs = (Subscriber<? super ResultMessage> resSubs) -> {

            try {
                int count = 0;//计数
                if (!resSubs.isUnsubscribed()) {
                    for (String param : params) {
                        count++;
                        System.out.println("第[" + count + "]次发送");
                        //发送参数
                        ResultMessage resMsg = restTemplate.getForObject(url, ResultMessage.class, param);
                        resSubs.onNext(resMsg);
                    }
                    //遍历所有参数后,发射完毕
                    resSubs.onCompleted();
                }
            } catch (Exception e) {
               resSubs.onError(e);
            }
        };
        return Observable.unsafeCreate(subs);
    }

    @Override
    protected Observable<ResultMessage> resumeWithFallback() {
        return Observable.error(new RuntimeException("发生了异常."));
    }


}
