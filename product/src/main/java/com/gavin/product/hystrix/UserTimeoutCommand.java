package com.gavin.product.hystrix;

import com.gavin.common.ResultMessage;
import com.netflix.hystrix.HystrixCommand;
import org.springframework.web.client.RestTemplate;

public class UserTimeoutCommand extends HystrixCommand<ResultMessage> {

    private RestTemplate restTemplate;

    /**
     * 构造方法
     * @param setter
     * @param restTemplate
     */
    public UserTimeoutCommand(Setter setter,RestTemplate restTemplate) {
        super(setter);
        this.restTemplate=restTemplate;
    }

    /**
     * 核心执行逻辑
     * @return
     * @throws Exception
     */
    @Override
    protected ResultMessage run() throws Exception {
        String url="http://USER/hystrix/timeout";
        return restTemplate.getForObject(url,ResultMessage.class);
    }

    /**
     * 降级方法
     * @return
     */
    @Override
    public ResultMessage getFallback() {
        return new ResultMessage(false,"请求超时了");
    }


}
