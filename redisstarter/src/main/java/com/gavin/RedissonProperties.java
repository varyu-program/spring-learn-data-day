package com.gavin;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Gavin
 * 这里的前缀即配置文件中的前缀
 */
@ConfigurationProperties(prefix = "gavin.redisson")
public class RedissonProperties {


    private String host ="localhost";
    private String password ;

    private int port =6379;

    private boolean ssl;

    private Integer timeOut;
    public String getHost() {
        return host;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }
}
