package com.gavin.distributemysql.utils;

import java.util.SortedMap;
import java.util.TreeMap;

public class ConsistentHashing {
    //用于存放数据源的哈希环
    private static final SortedMap<Integer, String> sortedMap = new TreeMap<>();

    public static void addDsKey(String dsKey) {
        for (int i = 0; i < 100; i++) {
            String key = dsKey + "#" + i;
            int hash = getHash(key);
            sortedMap.put(hash, dsKey);
        }
    }


    /**
     * 使用FNV1的32位哈希算法计算字符串Hash值
     *
     * @param string
     * @return
     */
    public static int getHash(String string) {
        final int p = 16777619;
        int hash = (int) 2166136261L;
        for (int i = 0; i < string.length(); i++) {
            hash = (hash ^ string.charAt(i)) * p;
        }
        hash += hash << 13;
        hash ^= hash >> 17;
        hash += hash << 3;
        hash ^= hash >> 17;
        hash += hash << 5;
        if (hash < 0) {
            hash = Math.abs(hash);
        }
        return hash;
    }

    public static String getDatasource(String node) {
        int hash = getHash(node);
        //得到大于该hash值的所有map
        SortedMap<Integer, String> subMap = sortedMap.tailMap(hash);
        Integer firstKey;
        //如果没有大于当前节点的哈希值的数据,就选择哈希值最小的数据源
        if (subMap == null || subMap.isEmpty()) {
            firstKey = sortedMap.firstKey();
        } else {
            //第一个key就是顺时针过去离字符串哈希值最近的那个节点
            firstKey = subMap.firstKey();
        }
        //返回服务器器名
        return sortedMap.get(firstKey);
    }
}
