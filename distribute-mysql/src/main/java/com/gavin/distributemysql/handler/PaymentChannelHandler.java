package com.gavin.distributemysql.handler;

import com.gavin.distributemysql.commons.PaymentChannelEnum;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.*;

/**
 * @author Gavin
 */
//定义需要转换的java类型
@MappedTypes(PaymentChannelEnum.class)
//定义转换的jdbc类型
@MappedJdbcTypes(JdbcType.INTEGER)
//上面两个注解告诉mybatis,该类型的转换器是转换PaymentChannelEnum 枚举跟Integer类型的

public class PaymentChannelHandler implements TypeHandler<PaymentChannelEnum> {
    @Override
    public void setParameter(PreparedStatement ps, int i, PaymentChannelEnum parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i,parameter.getId());
    }

    @Override
    public PaymentChannelEnum getResult(ResultSet rs, String columnName) throws SQLException {
        int rsInt = rs.getInt(columnName);
        return PaymentChannelEnum.getById(rsInt);
    }

    @Override
    public PaymentChannelEnum getResult(ResultSet rs, int columnIndex) throws SQLException {
        int rsInt = rs.getInt(columnIndex);
        return PaymentChannelEnum.getById(rsInt);
    }

    @Override
    public PaymentChannelEnum getResult(CallableStatement cs, int columnIndex) throws SQLException {
        int csInt = cs.getInt(columnIndex);
        return PaymentChannelEnum.getById(csInt);
    }
}
