package com.gavin.distributemysql.pojo;

import com.gavin.distributemysql.commons.PaymentChannelEnum;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;
@Data
@Alias("transaction") //定义Mybatis别名
public class Transaction implements Serializable {
    public static final long SID = 123456789L;
    private Long id;
    private Long userId;
    private Long productId;
    private PaymentChannelEnum paymentChannelEnum;
    private Date transDate;
    private Double amount;
    private Integer quantity;
    private Double discount;
    private String note;

}
