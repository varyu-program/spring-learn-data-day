package com.gavin.distributemysql.mapper;

import com.gavin.distributemysql.pojo.Transaction;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TransactionMapper {
    public List<Transaction> findTransaction(Long userId);
}
