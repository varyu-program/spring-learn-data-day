package com.gavin.distributemysql.config;

import com.ulisesbocchio.jasyptspringboot.annotation.EncryptablePropertySource;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Gavin
 * @Date 2023/6/19
 * @Description 自定义加密配置
 * @Version V1.0
 **/

@EncryptablePropertySource(name = "hmrs", value = "classpath:application.yml")
@Configuration
public class JasyptConfig {

    @Bean(name = "jasyptStringEncryptor")
    public StringEncryptor stringEncryptor() {
        PooledPBEStringEncryptor encrypt = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setAlgorithm("PBEWithMD5AndDES");
        config.setPassword("8mdHLTLyuOhEC9lYxcC6EQ==");
        config.setPoolSize(1);
        config.setKeyObtentionIterations(1000);
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setStringOutputType("sha256");
        encrypt.setConfig(config);
        return encrypt;
    }
}
