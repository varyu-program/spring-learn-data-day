package com.gavin.distributemysql.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @Author：Gavin
 * @name：ReadOnly
 * @Date：2023/6/20 10:14
 * @Filename：ReadOnly
 * @Desc: 配置只读注解
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ReadOnly {
}
