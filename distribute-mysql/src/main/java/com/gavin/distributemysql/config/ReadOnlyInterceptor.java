package com.gavin.distributemysql.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 @Author Gavin
 @Date 2023/6/20  
 @Description 切面优先级定义 优先级高于事务
 @Version V1.0
 **/
@Slf4j
@Aspect
@Component
public class ReadOnlyInterceptor  implements Ordered {


    @Around("@annotation(readOnly)")
    public Object setRead(ProceedingJoinPoint joinPoint, ReadOnly readOnly) throws Throwable{
        try{
            DbContextHolder.setDbType(DbContextHolder.READ);
            return joinPoint.proceed();
        }finally {
            DbContextHolder.clearDbType();
            log.info("使用了slave");
        }
    }
    @Around("@annotation(readWrite)")
    public Object setWrite(ProceedingJoinPoint joinPoint, ReadWrite readWrite) throws Throwable{
        try{
            DbContextHolder.setDbType(DbContextHolder.WRITE);
            return joinPoint.proceed();
        }finally {
            DbContextHolder.clearDbType();
            log.info("使用了 master");
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
