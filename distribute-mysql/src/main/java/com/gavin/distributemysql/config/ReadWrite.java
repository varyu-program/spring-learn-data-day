package com.gavin.distributemysql.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author：Gavin
 * @name：ReadWrite
 * @Date：2023/6/20 17:01
 * @Filename：ReadWrite
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ReadWrite {
}
