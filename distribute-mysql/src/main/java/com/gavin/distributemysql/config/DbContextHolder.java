package com.gavin.distributemysql.config;

import lombok.extern.slf4j.Slf4j;


/**
 @Author Gavin
 @Date 2023/6/20  
 @Description  切换读写模式 如果没有注解，则表示使用写模式。使用ReadOnly表示读模式
 @Version V1.0
 **/
@Slf4j
public class DbContextHolder {

    public static final String WRITE = "master";
    public static final String READ = "slave";

    private static ThreadLocal<String> contextHolder= new ThreadLocal<>();

    public static void setDbType(String dbType) {
        if (dbType == null) {
            log.error("dbType为空");
            throw new NullPointerException();
        }
        log.info("设置dbType为：{}",dbType);
        contextHolder.set(dbType);
    }

    public static String getDbType() {
        return contextHolder.get() == null ? WRITE : contextHolder.get();
    }

    public static void clearDbType() {
        contextHolder.remove();
    }

}
