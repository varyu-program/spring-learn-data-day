package com.gavin.distributemysql;

import com.gavin.distributemysql.utils.ConsistentHashing;
import com.gavin.snowflakeid.utils.SnowFlakeWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistributeMysqlApplication {

//    public static void main(String[] args) {
//        SpringApplication.run(DistributeMysqlApplication.class, args);
//    }

    public static void main(String[] args) {
        SnowFlakeWorker snowFlakeWorker = new SnowFlakeWorker(003);
        ConsistentHashing.addDsKey("001");
        ConsistentHashing.addDsKey("002");
        ConsistentHashing.addDsKey("003");
        for (int i = 0; i < 10; i++) {
            int[]dsCount={0,0,0};
            for (int j = 0; j < 1000; j++) {
                //获得id
                String id = snowFlakeWorker.nextId() + "";
                //获得hash
                int hash = ConsistentHashing.getHash(id);
                String datasource = ConsistentHashing.getDatasource(id);
                if ("001".equals(datasource)){
                    dsCount[0]++;
                }else if("002".equals(datasource)){
                    dsCount[1]++;
                }else if ("003".equals(datasource)){
                    dsCount[2]++;
                }
            }
            System.out.println(dsCount[0]+",\t"+dsCount[1]+",\t"+dsCount[2]);
        }
    }

}
