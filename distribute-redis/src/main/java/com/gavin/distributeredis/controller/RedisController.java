package com.gavin.distributeredis.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/session")
public class RedisController {

    @GetMapping("/set/{key}/{value}")
    public Map<String, String> setSessionAttribute(HttpServletRequest request, @PathVariable("key") String key, @PathVariable("value") String value) {
        Map<String, String> result = new HashMap<>();
        result.put(key, value);
        request.getSession().setAttribute(key, value);
        return result;
    }

    @GetMapping("/get/{key}")
    public Map<String, String> getSessionAttribute(HttpServletRequest request, @PathVariable("key") String key) {
        Map<String, String> result = new HashMap<>();
        String value =(String) request.getSession().getAttribute(key);
        result.put(key, value);
        request.getSession().setAttribute(key, value);
        return result;
    }

}
