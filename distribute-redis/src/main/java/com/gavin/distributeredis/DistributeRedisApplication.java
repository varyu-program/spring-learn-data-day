package com.gavin.distributeredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 3600)//启动 Spring SessionRedis,使得会话能够保存到Redis中了
@SpringBootApplication
public class DistributeRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributeRedisApplication.class, args);
    }

}
