package com.gavin.fund;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.gavin.*")
@EnableFeignClients(basePackages = "com.gavin.fund.*")//扫描OpenFeign客户端(即找到@FeignClient注解的类,一边能够抓鬼配到IOC容器中)
public class FundApplication {

    public static void main(String[] args) {
        SpringApplication.run(FundApplication.class, args);
    }

}
