package com.gavin.fund.service.impl;

import com.gavin.common.ResultMessage;
import com.gavin.fund.service.UserFacade;
import com.gavin.pojo.UserInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
public class UserFallBack  implements UserFacade {
    @Override
    public UserInfo getUser(Long id) {
        return new UserInfo(null,null,null);
    }

    @Override
    public UserInfo putUser(UserInfo userInfo) {
        return new UserInfo(null,null,null);
    }

    @Override
    public ResponseEntity<List<UserInfo>> findUser2(@RequestParam("ids") Long[] ids) {
        return null;
    }

    @Override
    public ResultMessage deleteUser(Long id) {
        return new ResultMessage(false,"服务降级");
    }

    @Override
    public ResultMessage uploadFile(MultipartFile file) {
        return new ResultMessage(false,"服务降级");

    }
}
