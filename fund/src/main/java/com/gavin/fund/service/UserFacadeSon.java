package com.gavin.fund.service;

import org.springframework.cloud.openfeign.FeignClient;

//@FeignClient("user")//声明为OpenFeign的客户端
/**
 * 由于继承了UserFacade,在UserFacadeSon上声明为OpenFeign客户端,那么
 * UserFacade也被Spring认为是一个客户端,这样在开发的时候可以不用改之前的接口,而是直接继承接口重新开发业务逻辑即可
 */
public interface UserFacadeSon extends UserFacade {
}
