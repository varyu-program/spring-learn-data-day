package com.gavin.fund.service;

import com.gavin.common.ResultMessage;
import com.gavin.fund.factory.UserFallBackFactory;
import com.gavin.fund.interceptor.UserInterceptor;
import com.gavin.fund.service.impl.UserFallBack;
import com.gavin.pojo.UserInfo;
import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.codec.Decoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@FeignClient(value = "user"
//        指定配置类
        , configuration = UserFacade.UserFeignConfig.class
//        降级方法
//        ,fallback = UserFallBack.class
        , fallbackFactory = UserFallBackFactory.class,contextId = "UserFacade")//声明为OpenFeign的客户端

//降级工厂和降级类之配置一个即可

public interface UserFacade {
    /**
     * 声明的方式类似于控制器
     *
     * @param id
     * @return
     */
    @GetMapping("/user/info/{id}")
    public UserInfo getUser(@PathVariable("id") Long id);

    @PutMapping("/user/info")
    public UserInfo putUser(@RequestBody UserInfo userInfo);

    @GetMapping("/user/infoes2")
    public ResponseEntity<List<UserInfo>> findUser2(@RequestParam("ids") Long[] ids);

    @DeleteMapping("/user/info")
    public ResultMessage deleteUser(@RequestHeader("id") Long id);

    //传递文件

    /**
     * consumes必须声明的文件格式
     * RequestPart 代表文件传递流
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/user/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResultMessage uploadFile(@RequestPart("file") MultipartFile file);

    //openfeign的配置类
    class UserFeignConfig {

        @Autowired
        private ObjectFactory<HttpMessageConverters> messageConvertersObjectFactory;

        @Bean(name = "feignDecoder")//此处bean的名称要和默认的装配保持一致
        //设置bean作用范围,
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public Decoder clientDecoder() {
            return new SpringDecoder(messageConvertersObjectFactory);
        }

        /**
         * 配置拦截器
         *
         * @return
         */
        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public RequestInterceptor userInterceptor() {
            return new UserInterceptor();
        }

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        Logger.Level loggerLevel() {
            return Logger.Level.FULL;
        }

        /**
         * 自定义openfeign客户端
         *
         * @return
         */
        @Bean(name = "feignBuilder")
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public Feign.Builder clientBuilder() {
            return Feign.builder();
        }
    }
}
