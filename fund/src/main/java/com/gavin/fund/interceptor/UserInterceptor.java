package com.gavin.fund.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class UserInterceptor implements RequestInterceptor {
    /**
     * 可以根据需要限制请求,比如要带token才能请求等
     * @param requestTemplate
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("token","token");
    }
}
