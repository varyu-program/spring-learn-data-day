package com.gavin.fund.factory;

import com.gavin.common.ResultMessage;
import com.gavin.fund.service.UserFacade;
import com.gavin.pojo.UserInfo;
import feign.hystrix.FallbackFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
public class UserFallBackFactory  implements FallbackFactory {
    @Override
    public UserFacade create(Throwable throwable) {
        return new UserFacade() {
            private Long ERROR_ID=Long.MAX_VALUE;
            @Override
            public UserInfo getUser(Long id) {
                return new UserInfo(ERROR_ID,null,throwable.getMessage());
            }

            @Override
            public UserInfo putUser(UserInfo userInfo) {
                return new UserInfo(ERROR_ID,null,throwable.getMessage());

            }

            @Override
            public ResponseEntity<List<UserInfo>> findUser2(@RequestParam("ids") Long[] ids) {
                return null;
            }

            @Override
            public ResultMessage deleteUser(Long id) {
                return new ResultMessage(false,throwable.getMessage());
            }

            @Override
            public ResultMessage uploadFile(MultipartFile file) {
                return new ResultMessage(false,throwable.getMessage());

            }
        };
    }
}
