package com.gavin.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 循环依赖问题没解决
 */
@EnableZuulProxy //驱动注册zuul
@SpringBootApplication(scanBasePackages = "com.gavin.*")
public class ZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class, args);
    }

}
