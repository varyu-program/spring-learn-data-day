package com.gavin.distributesecurity.pojo;

import lombok.*;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Alias("role")
public class RolePo implements Serializable {

    private Long id;
    private String roleName;
    private String note;

    private static final long serialVersionUID = -10L;


}
