package com.gavin.distributesecurity.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.List;

@Data
@Alias("userRole")
public class UserRolePo implements Serializable {
    private UserPo user;
    private List<RolePo> roleList;
    private static final long serialVersionUID = -12L;
}
