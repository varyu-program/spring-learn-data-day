package com.gavin.distributesecurity.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Alias("user")
public class UserPo  implements Serializable {
    private Long id;
    private String userName;
    private String password;
    private boolean available;
    private String note;
    private static final long serialVersionUID = -11L;

}
