package com.gavin.distributesecurity;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 3600)
@SpringBootApplication(scanBasePackages = "com.gavin.*")
@MapperScan(basePackages = "com.gavin.*" ,annotationClass = Mapper.class)
public class DistributeSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributeSecurityApplication.class, args);
        System.out.println(new BCryptPasswordEncoder().encode("123456"));
    }
}
