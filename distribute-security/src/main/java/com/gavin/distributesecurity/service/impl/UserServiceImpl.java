package com.gavin.distributesecurity.service.impl;

import com.gavin.distributesecurity.mapper.RoleMapper;
import com.gavin.distributesecurity.mapper.UserMapper;
import com.gavin.distributesecurity.pojo.RolePo;
import com.gavin.distributesecurity.pojo.UserPo;
import com.gavin.distributesecurity.pojo.UserRolePo;
import com.gavin.distributesecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserRolePo getUserRole(Long userId) {
        UserRolePo userRolePo = new UserRolePo();
        UserPo user = userMapper.getUser(userId);
        List<RolePo> roleList = roleMapper.findRolesByUserId(userId);
        userRolePo.setUser(user);
        userRolePo.setRoleList(roleList);
        return userRolePo;

    }

    @Override
    public UserRolePo getUserRoleByUserName(String userName) {
        UserRolePo userRolePo = new UserRolePo();
        UserPo userPo = userMapper.getUserByUserName(userName);
        List<RolePo> roleList = roleMapper.findRolesByUserId(userPo.getId());
        userRolePo.setUser(userPo);
        userRolePo.setRoleList(roleList);
        return userRolePo;
    }


}
