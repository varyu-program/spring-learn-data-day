package com.gavin.distributesecurity.service.impl;

import com.gavin.distributesecurity.pojo.RolePo;
import com.gavin.distributesecurity.pojo.UserPo;
import com.gavin.distributesecurity.pojo.UserRolePo;
import com.gavin.distributesecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserRolePo userRolePo = userService.getUserRoleByUserName(s);
        return change(userRolePo);
    }

    public UserDetails change(UserRolePo userRolePo) {
        //权限列表
        List<GrantedAuthority> authorityList = new ArrayList<>();
        //用户角色
        List<RolePo> roleList = userRolePo.getRoleList();
        for (RolePo role : roleList) {
            //将角色名称放入权限列表中
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role.getRoleName());
            authorityList.add(simpleGrantedAuthority);
        }
        UserPo user = userRolePo.getUser();
       //创建spring security用户信息,当然 也可以通过userpo实现spring security的Userdetails接口来实现
        User result = new User(user.getUserName(), user.getPassword(), authorityList);
        return result;
    }

}
