package com.gavin.distributesecurity.service;

import com.gavin.distributesecurity.pojo.UserRolePo;

public interface UserService {
    UserRolePo getUserRole(Long userId);

    UserRolePo getUserRoleByUserName(String userName);
}
