package com.gavin.distributesecurity.mapper;

import com.gavin.distributesecurity.pojo.UserPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {

    public UserPo getUser(@Param("id") Long id);

    public UserPo getUserByUserName(@Param("userName") String userName);
}
