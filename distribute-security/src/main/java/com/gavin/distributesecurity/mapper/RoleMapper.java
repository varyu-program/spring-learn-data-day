package com.gavin.distributesecurity.mapper;

import com.gavin.distributesecurity.pojo.RolePo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleMapper {

    public List<RolePo> findRolesByUserId(@Param("userId") Long userId);
}