package com.gavin.distributesecurity.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {

//    @PreAuthorize("hasAuthority('ROLE_USER')")
    @GetMapping("/user/get")
    public String userGet() {
        return "user-get";
    }
//    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/admin/get")
    public String adminGet() {
        return "admin-get";
    }
}
