package com.gavin.user.userServer;

import com.gavin.common.ResultMessage;
import com.gavin.pojo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserInfoController {

    @GetMapping("/info/{id}")
    public UserInfo getUser(@PathVariable("id") Long id) {
        UserInfo userInfo = new UserInfo(id, "user_name" + id, "note_" + id);
        return userInfo;

    }


    @PutMapping("/info")
    public UserInfo putUser(@RequestBody UserInfo userInfo) {
        return userInfo;
    }

    /**
     * 查找用户信息
     *
     * @param ids
     * @return
     */
    @GetMapping("/infoes/{ids}")
    public ResponseEntity<List<UserInfo>> findUsers(@PathVariable("ids") Long[] ids) {
        List<UserInfo> userList = new ArrayList<>();
        for (Long id : ids) {
            UserInfo userInfo = new UserInfo(id, "user_name_" + id, "note_" + id);
            userList.add(userInfo);
        }
        ResponseEntity<List<UserInfo>> listResponseEntity = new ResponseEntity<>(userList, HttpStatus.OK);
        return listResponseEntity;
    }


    @GetMapping("/infoes2")
    public ResponseEntity<List<UserInfo>> findUser2(@RequestParam("ids") Long[] ids) {
        List<UserInfo> userList = new ArrayList<>();
        for (Long id : ids) {
            UserInfo userInfo = new UserInfo(id, "user_name_" + id, "note_" + id);
            userList.add(userInfo);
        }
        ResponseEntity<List<UserInfo>> response = new ResponseEntity<>(userList, HttpStatus.OK);
        return response;
    }


    @DeleteMapping("/info")
    public ResultMessage deleteUser(@RequestHeader("id") Long id) {
        boolean success = id != null;
        String msg = success ? "参数传递成功" : "参数传递失败";
        return new ResultMessage(success, msg);
    }


    @PostMapping(value = "upload")
    public ResultMessage uploadFile(@RequestPart("file")MultipartFile file){
        boolean success = file != null && file.getSize() > 0;
        String msg = success ? "文件传递成功" : "文件传递失败";
        return new ResultMessage(success, msg);
    }
}
