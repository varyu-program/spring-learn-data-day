package com.gavin.user.userServer;


import com.gavin.common.ResultMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hystrix")
public class UserController {
    private static Long MAX_SLEEP_TIME = 5000L;

    @GetMapping("/timeout")
    public ResultMessage timeout() {
        Long sleepTime = (long) (MAX_SLEEP_TIME * Math.random());
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            System.out.println("执行异常");
            e.printStackTrace();
        }
        return new ResultMessage(true, "执行时间" + sleepTime);
    }

    @GetMapping("/exp/{msg}")
    public ResultMessage exp(@PathVariable("msg") String msg) {
        if ("spring".equals(msg)) {
            return new ResultMessage(true, msg);
        } else {
            throw new RuntimeException("出现了异常,请检查参数msg是否spring");
        }

    }


}
