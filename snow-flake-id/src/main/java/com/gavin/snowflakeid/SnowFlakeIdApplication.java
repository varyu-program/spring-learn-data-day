package com.gavin.snowflakeid;

import com.gavin.snowflakeid.utils.CustomWorker;
import com.gavin.snowflakeid.utils.SnowFlakeWorker;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
public class SnowFlakeIdApplication {

//    public static void main(String[] args) {
//        SpringApplication.run(SnowFlakeIdApplication.class, args);
//    }

    public static void main(String[] args) {
//        for (int i = 0; i < 100; i++) {
//            long nextId = new SnowFlakeWorker(new Random().nextInt(1024)).nextId();
//            System.out.println(nextId);
//        }
        for (int i = 0; i < 100; i++) {
            long nextId = new CustomWorker(new Random().nextInt(127)).nextId();
            System.out.println(nextId);
        }
    }
}
