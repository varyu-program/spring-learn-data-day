package com.gavin.configclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigClientController {
    //读取文件配置信息

    @Value("${version.message}")
    private String versionMsg;

    @GetMapping("/version/message")
    public String versionMessage() {
        return versionMsg;
    }

}
