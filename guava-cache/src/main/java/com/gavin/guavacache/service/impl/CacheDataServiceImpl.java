package com.gavin.guavacache.service.impl;

import com.gavin.guavacache.common.CacheData;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CacheDataServiceImpl {

    private Cache<Integer, CacheData> dataCache = CacheBuilder.newBuilder()//使用cachebuilder创建普通的缓存
            .expireAfterAccess(Duration.ofMinutes(30))//缓存失效时间
            .maximumSize(10240)//缓存过期时间,在每次访问的半小时后过期
            .expireAfterWrite(Duration.ofHours(2))//在每次写入之后2h过期
            .concurrencyLevel(4)//指定并发修改数量,默认4
            .initialCapacity(2048)//设置初始化大小,避免经常扩容
            .build();


    private LoadingCache<Integer, CacheData> dataLoadingCache = CacheBuilder.newBuilder()
            .expireAfterAccess(Duration.ofMinutes(30)).
                    initialCapacity(1024)
            .maximumSize(10240)
            .refreshAfterWrite(Duration.ofHours(2))
            .build(new CacheLoader<Integer, CacheData>() {
                //加载缓存的方法
                @Override
                public CacheData load(Integer integer) throws Exception {
                    return getDataFromDataSource(integer);
                }


                //批量查询的方法,如果没用批量查询,则可以不用重写
                @Override
                public Map<Integer, CacheData> loadAll(Iterable<? extends Integer> keys) throws Exception {
                    ArrayList<? extends Integer> ids = Lists.newArrayList(keys.iterator());
                    List<CacheData> dataList = listDataByIds(ids);
                    return dataList.stream().collect(Collectors.toMap(CacheData::getId, Function.identity()));
                }
            });


    private CacheData getDataFromDataSource(Integer id) {
        CacheData data = new CacheData(id, "数据库数据");
        return data;
    }

    private List<CacheData> listDataByIds(List ids) {
        List<CacheData> dataList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            dataList.add(new CacheData(i, "数据库数据" + i));
        }
        return dataList;
    }


    private CacheData getDataByIdFromCache(Integer id){

        try {
            return dataCache.get(id,()->getDataFromDataSource(id));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return getDataFromDataSource(id);
    }
}
